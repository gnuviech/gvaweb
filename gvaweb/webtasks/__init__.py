"""
This module contains :py:mod:`webtasks.tasks`.

"""

__version__ = "0.4.0"

from webtasks.celery import app as celery_app

__all__ = ("celery_app",)
