"""
This module defines the Celery_ app for gvaweb.

.. _Celery: http://www.celeryproject.org/

"""
from celery import Celery

#: The Celery application
app = Celery("webtasks")

app.config_from_object("webtasks.settings", namespace="CELERY")
app.autodiscover_tasks(["webtasks.tasks"], force=True)
