Changelog
=========

* :release:`0.4.0 <2023-05-08>`
* :support:`-` switch from Pipenv to Poetry

* :release:`0.3.0 <2020-04-10>`
* :support:`-` update Vagrant setup to libvirt and Debian Buster

* :release:`0.2.0 <2020-03-03>`
* :support:`-` add Docker setup for lightweight local testing
* :support:`-` restructure code to have celery in the webtasks module and drop
  the gvaldap module
* :support:`-` update documentation to alabaster theme

* :release:`0.1.8 <2020-03-02>`
* :bug:`-` salt automation was broken due to changed paths and missing Jinja 2
  dependency

* :release:`0.1.7 <2020-03-02>`
* :bug:`-` fix package paths

* :release:`0.1.6 <2020-03-02>`
* :bug:`-` remove disfunctional code in create_web_php_fpm_pool_config

* :release:`0.1.5 <2020-02-29>`
* :bug:`-` fix path to systemctl
* :bug:`-` reduce template for nginx vhosts to the bare minimum to use
  letsencrypt’s certbot

* :release:`0.1.4 <2019-09-07>`
* :bug:`-` remove handling of php-fpm reloads
* :bug:`-` use systemctl to reload nginx

* :release:`0.1.3 <2019-09-07>`
* :bug:`-` fix handling of manually removed configuration files

* :release:`0.1.2 <2019-06-30>`
* :bug:`5` ignore missing site configuration when disabling sites
* :bug:`-` add listen directive to enable IPv6

* :release:`0.1.1 <2015-01-27>`
* :bug:`-` force symlink creation for enable_web_vhost task to make it
  idempotent

* :release:`0.1.0 <2015-01-27>`
* :feature:`-` add tasks to setup and delete per user PHP5 FPM pool
  configurations
* :feature:`-` add tasks to setup, enable, disable and delete nginx virtual
  host configurations
* :support:`-` initial project setup
