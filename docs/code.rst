==================
Code documentation
==================

gvaweb is implemented as `Celery`_ app.

.. _Celery: http://www.celeryproject.org/


The module :py:mod:`webtasks`
=============================

.. automodule:: webtasks


:py:mod:`celery <webtasks.celery>`
----------------------------------

.. automodule:: webtasks.celery
   :members:


:py:mod:`settings <webtasks.settings>`
--------------------------------------

.. automodule:: webtasks.settings


:py:mod:`tasks <webtasks.tasks>`
--------------------------------

.. automodule:: webtasks.tasks
   :members:
   :undoc-members:

