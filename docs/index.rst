.. gvaweb documentation master file, created by
   sphinx-quickstart on Mon Jan 26 15:51:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Welcome to gvaweb's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   deploy
   code
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

